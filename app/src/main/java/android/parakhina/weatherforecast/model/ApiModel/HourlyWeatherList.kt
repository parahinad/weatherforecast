package android.parakhina.weatherforecast.model.ApiModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class HourlyWeatherList {

    @SerializedName("dt")
    @Expose
    private var dt: Int? = null

    @SerializedName("main")
    @Expose
    private var main: Main? = null

    @SerializedName("weather")
    @Expose
    private var weather: List<Weather?>? = null

    @SerializedName("clouds")
    @Expose
    private var clouds: Clouds? = null

    @SerializedName("wind")
    @Expose
    private var wind: Wind? = null

    @SerializedName("sys")
    @Expose
    private var sys: Sys? = null

    @SerializedName("dt_txt")
    @Expose
    private var dtTxt: String? = null

    @SerializedName("rain")
    @Expose
    private var rain: Rain? = null

    @SerializedName("snow")
    @Expose
    private var snow: Snow? = null

    fun getDt(): Int? {
        return dt
    }

    fun setDt(dt: Int?) {
        this.dt = dt
    }

    fun getMain(): Main? {
        return main
    }

    fun setMain(main: Main?) {
        this.main = main
    }

    fun getWeather(): List<Weather?>? {
        return weather
    }

    fun setWeather(weather: List<Weather?>?) {
        this.weather = weather
    }

    fun getClouds(): Clouds? {
        return clouds
    }

    fun setClouds(clouds: Clouds?) {
        this.clouds = clouds
    }

    fun getWind(): Wind? {
        return wind
    }

    fun setWind(wind: Wind?) {
        this.wind = wind
    }

    fun getSys(): Sys? {
        return sys
    }

    fun setSys(sys: Sys?) {
        this.sys = sys
    }

    fun getDtTxt(): String? {
        return dtTxt
    }

    fun setDtTxt(dtTxt: String?) {
        this.dtTxt = dtTxt
    }

    fun getRain(): Rain? {
        return rain
    }

    fun setRain(rain: Rain?) {
        this.rain = rain
    }

    fun getSnow(): Snow? {
        return snow
    }

    fun setSnow(snow: Snow?) {
        this.snow = snow
    }
}