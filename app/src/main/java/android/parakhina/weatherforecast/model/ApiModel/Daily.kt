package android.parakhina.weatherforecast.model.ApiModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Daily {
    @SerializedName("dt")
    @Expose
    private var dt: Int? = null

    @SerializedName("sunrise")
    @Expose
    private var sunrise: Int? = null

    @SerializedName("sunset")
    @Expose
    private var sunset: Int? = null

    @SerializedName("temp")
    @Expose
    private var temp: Temp? = null

    @SerializedName("feels_like")
    @Expose
    private var feelsLike: FeelsLike? = null

    @SerializedName("pressure")
    @Expose
    private var pressure: Int? = null

    @SerializedName("humidity")
    @Expose
    private var humidity: Int? = null

    @SerializedName("dew_point")
    @Expose
    private var dewPoint: Double? = null

    @SerializedName("wind_speed")
    @Expose
    private var windSpeed: Double? = null

    @SerializedName("wind_deg")
    @Expose
    private var windDeg: Int? = null

    @SerializedName("weather")
    @Expose
    private var weather: List<Weather?>? = null

    @SerializedName("clouds")
    @Expose
    private var clouds: Int? = null

    @SerializedName("pop")
    @Expose
    private var pop: Double? = null

    @SerializedName("rain")
    @Expose
    private var rain: Double? = null

    @SerializedName("uvi")
    @Expose
    private var uvi: Double? = null

    fun getDt(): Int? {
        return dt
    }

    fun setDt(dt: Int?) {
        this.dt = dt
    }

    fun getSunrise(): Int? {
        return sunrise
    }

    fun setSunrise(sunrise: Int?) {
        this.sunrise = sunrise
    }

    fun getSunset(): Int? {
        return sunset
    }

    fun setSunset(sunset: Int?) {
        this.sunset = sunset
    }

    fun getTemp(): Temp? {
        return temp
    }

    fun setTemp(temp: Temp?) {
        this.temp = temp
    }

    fun getFeelsLike(): FeelsLike? {
        return feelsLike
    }

    fun setFeelsLike(feelsLike: FeelsLike?) {
        this.feelsLike = feelsLike
    }

    fun getPressure(): Int? {
        return pressure
    }

    fun setPressure(pressure: Int?) {
        this.pressure = pressure
    }

    fun getHumidity(): Int? {
        return humidity
    }

    fun setHumidity(humidity: Int?) {
        this.humidity = humidity
    }

    fun getDewPoint(): Double? {
        return dewPoint
    }

    fun setDewPoint(dewPoint: Double?) {
        this.dewPoint = dewPoint
    }

    fun getWindSpeed(): Double? {
        return windSpeed
    }

    fun setWindSpeed(windSpeed: Double?) {
        this.windSpeed = windSpeed
    }

    fun getWindDeg(): Int? {
        return windDeg
    }

    fun setWindDeg(windDeg: Int?) {
        this.windDeg = windDeg
    }

    fun getWeather(): List<Weather?>? {
        return weather
    }

    fun setWeather(weather: List<Weather?>?) {
        this.weather = weather
    }

    fun getClouds(): Int? {
        return clouds
    }

    fun setClouds(clouds: Int?) {
        this.clouds = clouds
    }

    fun getPop(): Double? {
        return pop
    }

    fun setPop(pop: Double?) {
        this.pop = pop
    }

    fun getRain(): Double? {
        return rain
    }

    fun setRain(rain: Double?) {
        this.rain = rain
    }

    fun getUvi(): Double? {
        return uvi
    }

    fun setUvi(uvi: Double?) {
        this.uvi = uvi
    }

}