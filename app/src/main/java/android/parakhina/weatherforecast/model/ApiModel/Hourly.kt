package android.parakhina.weatherforecast.model.ApiModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Hourly {
    @SerializedName("dt")
    @Expose
    private var dt: Int? = null

    @SerializedName("temp")
    @Expose
    private var temp: Double? = null

    @SerializedName("feels_like")
    @Expose
    private var feelsLike: Double? = null

    @SerializedName("pressure")
    @Expose
    private var pressure: Int? = null

    @SerializedName("humidity")
    @Expose
    private var humidity: Int? = null

    @SerializedName("dew_point")
    @Expose
    private var dewPoint: Double? = null

    @SerializedName("clouds")
    @Expose
    private var clouds: Int? = null

    @SerializedName("visibility")
    @Expose
    private var visibility: Int? = null

    @SerializedName("wind_speed")
    @Expose
    private var windSpeed: Double? = null

    @SerializedName("wind_deg")
    @Expose
    private var windDeg: Int? = null

    @SerializedName("weather")
    @Expose
    private var weather: List<Weather?>? = null

    @SerializedName("pop")
    @Expose
    private var pop: Double? = null

    @SerializedName("rain")
    @Expose
    private var rain: Rain? = null

    fun getDt(): Int? {
        return dt
    }

    fun setDt(dt: Int?) {
        this.dt = dt
    }

    fun getTemp(): Double? {
        return temp
    }

    fun setTemp(temp: Double?) {
        this.temp = temp
    }

    fun getFeelsLike(): Double? {
        return feelsLike
    }

    fun setFeelsLike(feelsLike: Double?) {
        this.feelsLike = feelsLike
    }

    fun getPressure(): Int? {
        return pressure
    }

    fun setPressure(pressure: Int?) {
        this.pressure = pressure
    }

    fun getHumidity(): Int? {
        return humidity
    }

    fun setHumidity(humidity: Int?) {
        this.humidity = humidity
    }

    fun getDewPoint(): Double? {
        return dewPoint
    }

    fun setDewPoint(dewPoint: Double?) {
        this.dewPoint = dewPoint
    }

    fun getClouds(): Int? {
        return clouds
    }

    fun setClouds(clouds: Int?) {
        this.clouds = clouds
    }

    fun getVisibility(): Int? {
        return visibility
    }

    fun setVisibility(visibility: Int?) {
        this.visibility = visibility
    }

    fun getWindSpeed(): Double? {
        return windSpeed
    }

    fun setWindSpeed(windSpeed: Double?) {
        this.windSpeed = windSpeed
    }

    fun getWindDeg(): Int? {
        return windDeg
    }

    fun setWindDeg(windDeg: Int?) {
        this.windDeg = windDeg
    }

    fun getWeather(): List<Weather?>? {
        return weather
    }

    fun setWeather(weather: List<Weather?>?) {
        this.weather = weather
    }

    fun getPop(): Double? {
        return pop
    }

    fun setPop(pop: Double?) {
        this.pop = pop
    }

    fun getRain(): Rain? {
        return rain
    }

    fun setRain(rain: Rain?) {
        this.rain = rain
    }
}