package android.parakhina.weatherforecast.model

import android.content.Context
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User {

    private var uId: String? = null
    private var uCity: String? = null

    companion object {
        private var mInstance : User? = null

        fun  getInstance(): User {
            if (mInstance == null)
                mInstance = User()

            return mInstance!!
        }
    }

    fun getId(): String? {
        return uId
    }

    fun setId(id: String): User? {
        uId = id
        return mInstance
    }

    fun getCity(): String?{
        return uCity
    }
    fun setCity(city: String): User?{
        uCity = city
        return mInstance
    }
}