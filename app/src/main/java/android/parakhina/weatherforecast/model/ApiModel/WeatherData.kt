package android.parakhina.weatherforecast.model.ApiModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class WeatherData {
    @SerializedName("coord")
    @Expose
    var coord: Coord? = null

    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null

    @SerializedName("base")
    @Expose
    var base: String? = null

    @SerializedName("main")
    @Expose
    var main: Main? = null

    @SerializedName("visibility")
    @Expose
    var visibility: Int? = null

    @SerializedName("wind")
    @Expose
    var wind: Wind? = null

    @SerializedName("clouds")
    @Expose
    var clouds: Clouds? = null

    @SerializedName("dt")
    @Expose
    var dt: Int? = null

    @SerializedName("sys")
    @Expose
    var sys: Sys? = null

    @SerializedName("timezone")
    @Expose
    var timezone: String? = null

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("cod")
    @Expose
    var cod: Int? = null

    @SerializedName("list")
    @Expose
    var list: List<HourlyWeatherList>? = null

    @SerializedName("lat")
    @Expose
    private var lat: Double? = null

    @SerializedName("lon")
    @Expose
    private var lon: Double? = null


    @SerializedName("timezone_offset")
    @Expose
    private var timezoneOffset: Int? = null

    @SerializedName("current")
    @Expose
    private var current: Current? = null


    @SerializedName("hourly")
    @Expose
    private var hourly: List<Hourly?>? = null

    @SerializedName("daily")
    @Expose
    private var daily: List<Daily?>? = null

    @SerializedName("city")
    @Expose
    private var city: City? = null


    fun getLat(): Double? {
        return lat
    }

    fun setLat(lat: Double?) {
        this.lat = lat
    }

    fun getLon(): Double? {
        return lon
    }

    fun setLon(lon: Double?) {
        this.lon = lon
    }

    fun getTimezoneOffset(): Int? {
        return timezoneOffset
    }

    fun setTimezoneOffset(timezoneOffset: Int?) {
        this.timezoneOffset = timezoneOffset
    }

    fun getCurrent(): Current? {
        return current
    }

    fun setCurrent(current: Current?) {
        this.current = current
    }

    fun getHourly(): List<Hourly?>? {
        return hourly
    }

    fun setHourly(hourly: List<Hourly?>?) {
        this.hourly = hourly
    }

    fun getDaily(): List<Daily?>? {
        return daily
    }

    fun setDaily(daily: List<Daily?>?) {
        this.daily = daily
    }
    fun getCity(): City? {
        return city
    }

    fun setCity(city: City?) {
        this.city = city
    }

}