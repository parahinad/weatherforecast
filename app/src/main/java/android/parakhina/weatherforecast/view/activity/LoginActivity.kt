package android.parakhina.weatherforecast.view.activity

import android.content.Intent
import android.os.Bundle
import android.parakhina.weatherforecast.R
import android.parakhina.weatherforecast.localstorage.DataBaseHandler
import android.parakhina.weatherforecast.localstorage.WeatherLocal
import android.parakhina.weatherforecast.model.User
import android.parakhina.weatherforecast.support.*
import android.parakhina.weatherforecast.support.EMAIL
import android.parakhina.weatherforecast.support.PROFILE
import android.parakhina.weatherforecast.support.RC_SIGN_IN
import android.parakhina.weatherforecast.support.TABLE_TEMP
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.*


@Suppress("DEPRECATION")
class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var mGoogleSignInClient : GoogleSignInClient
    private lateinit var gso : GoogleSignInOptions
    private val TAG = LoginActivity::class.qualifiedName
    private lateinit var signInMethod: String
    private lateinit var callbackManager: CallbackManager
    private var connection: CheckConnection? = null
    private lateinit var db: DataBaseHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
        setContentView(R.layout.activity_login)
        db = DataBaseHandler(this)
        init()
    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        connection = CheckConnection()
        if(!connection?.hasConnection(this@LoginActivity)!!){
            val data = db.readData(TABLE_USER)
            for (i in 0 until data.size) {
                User.getInstance().setId(data[i].uId.toString())
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }else {
            val currentUser = auth.currentUser
            updateUI(currentUser)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(signInMethod == "facebook")
            callbackManager.onActivityResult(requestCode, resultCode, data)
        else if(signInMethod == "google") {
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)!!
                    Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                    firebaseAuthWithGoogle(account.idToken!!)
                } catch (e: ApiException) {
                    // Google Sign In failed, update UI appropriately
                    Log.w(TAG, "Google sign in failed", e)
                    // ...
                }
            }
        }
    }

    private fun init(){
        auth = Firebase.auth

        val signInGoogleBtn = findViewById<View>(R.id.google_login_button) as SignInButton
        signInGoogleBtn.setOnClickListener {
            signInMethod = "google"
            signIn()
        }

        val signInFBBtn = findViewById<View>(R.id.facebook_login_button) as LoginButton
        signInFBBtn.setOnClickListener {
            signInMethod = "facebook"
            signIn()
        }

    }

    private fun signIn() {
        lateinit var signInIntent: Intent
        if (signInMethod == "google") {
            createRequest()
            signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        } else if (signInMethod == "facebook"){
            facebookLogin()
        }
    }

    private fun createRequest() {
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun facebookLogin(){
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().logInWithReadPermissions(
            this, listOf(
                PROFILE,
                EMAIL
            )
        )
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    firebaseAuthWithFacebook(loginResult)
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    // App code
                }
            })

    }

    private fun firebaseAuthWithFacebook(loginResult: LoginResult?){
        val credential = FacebookAuthProvider.getCredential(loginResult?.accessToken?.token!!)
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener{ task ->
            if(task.isSuccessful){
                Log.d(TAG, "signInWithCredential:success")
                val user = auth.currentUser
                updateUI(user)
            }else{
                Log.w(TAG, "signInWithCredential:failure", task.exception)
                Toast.makeText(this, "Authentication Failed.", Toast.LENGTH_SHORT).show()
                updateUI(null)
            }
        }

    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    // ...
                    Toast.makeText(this, "Authentication Failed.", Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }

                // ...
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null){
            User.getInstance().setId(user.uid)
            val uId = WeatherLocal(User.getInstance().getId(), false)
            db.deleteData(TABLE_USER)
            db.insertData(uId, TABLE_USER)
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

}