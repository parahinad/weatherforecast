package android.parakhina.weatherforecast.view.adapter

import android.R
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import java.util.*
import kotlin.collections.ArrayList


@Suppress("NAME_SHADOWING")
class ExpListAdapter(context: Context, label: ArrayList<ArrayList<String?>>, value: ArrayList<ArrayList<String?>>) :
    BaseExpandableListAdapter() {
    private val mLabel: ArrayList<ArrayList<String?>> = label
    private val mValue: ArrayList<ArrayList<String?>> = value
    private val mContext: Context = context
    override fun getGroupCount(): Int {
        return mLabel.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return mLabel[groupPosition].size
    }

    override fun getGroup(groupPosition: Int): Any {
        return mLabel[groupPosition]
    }

    override fun getChild(groupPosition: Int, childPosition: Int): String? {
        return mLabel[groupPosition][childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    @SuppressLint("InflateParams")
    override fun getGroupView(
        groupPosition: Int, isExpanded: Boolean, convertView: View?,
        parent: ViewGroup?
    ): View? {
        var convertView: View? = convertView
        if (convertView == null) {
            val inflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(android.parakhina.weatherforecast.R.layout.group_view, null)
        }
        val textGroup = convertView?.findViewById(android.parakhina.weatherforecast.R.id.textGroup) as TextView
        val myOptions = ArrayList<String>(listOf(*mContext.resources.getStringArray(android.parakhina.weatherforecast.R.array.weather_data_label_array)))
        textGroup.text = myOptions[groupPosition]
        return convertView
    }

    @SuppressLint("InflateParams")
    override fun getChildView(
        groupPosition: Int, childPosition: Int, isLastChild: Boolean,
        convertView: View?, parent: ViewGroup?
    ): View? {
        var convertView: View? = convertView
        if (convertView == null) {
            val inflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(android.parakhina.weatherforecast.R.layout.child_view, null)
        }
        val textChildLabel = convertView?.findViewById(android.parakhina.weatherforecast.R.id.textChildLabel) as TextView
        textChildLabel.text = mLabel[groupPosition][childPosition]
        val textChildValue = convertView.findViewById(android.parakhina.weatherforecast.R.id.textChildValue) as TextView
        textChildValue.text = mValue[groupPosition][childPosition].toString()
        return convertView
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

}