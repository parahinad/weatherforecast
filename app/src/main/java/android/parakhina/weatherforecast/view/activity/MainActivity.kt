package android.parakhina.weatherforecast.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.parakhina.weatherforecast.BuildConfig
import android.parakhina.weatherforecast.R
import android.parakhina.weatherforecast.api.ApiService
import android.parakhina.weatherforecast.api.RetroClient
import android.parakhina.weatherforecast.firestore.UserData
import android.parakhina.weatherforecast.localstorage.DataBaseHandler
import android.parakhina.weatherforecast.localstorage.WeatherLocal
import android.parakhina.weatherforecast.model.ApiModel.HourlyWeatherList
import android.parakhina.weatherforecast.model.ApiModel.WeatherData
import android.parakhina.weatherforecast.model.User
import android.parakhina.weatherforecast.support.*
import android.parakhina.weatherforecast.view.adapter.ExpListAdapter
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.facebook.login.LoginManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MainActivity : AppCompatActivity() {

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLastLocation: Location? = null
    private val TAG = "MainActivity"

    private lateinit var auth: FirebaseAuth
    private lateinit var cityTextView: TextView
    private lateinit var searchBtn: ImageButton
    private lateinit var searchEditText: EditText
    private lateinit var apiService: ApiService
    private var mLabels: ArrayList<ArrayList<String?>>? = null
    private var mValues: ArrayList<ArrayList<String?>>? = null

    private var isDaily = false
    private var userData: UserData? = null
    private var connection: CheckConnection? = null
    private lateinit var db: DataBaseHandler
    private var labels: ArrayList<String?>? = null
    private var values: ArrayList<String?>? = null
    private var getLocation: GetPermission? = null
    private var isFirstOpen = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        apiService = RetroClient.create()
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        db = DataBaseHandler(this)
        init()
    }


    private fun init() {
        auth = Firebase.auth
        getToken()
        userData = UserData()
        connection = CheckConnection()
        getLocation = GetPermission(this, this@MainActivity)
        mLabels = ArrayList()
        mValues = ArrayList()
        labels = ArrayList()
        values = ArrayList()

        val spinner: Spinner = findViewById(R.id.spinner_nav)
        ArrayAdapter.createFromResource(
            this,
            R.array.weather_display_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        val data = db.readData(TABLE_USER)
        for (i in 0 until data.size) {
            if (User.getInstance().getId() == data[i].uId) {
                isDaily = data[i].isDaily
            }
        }
        if (isDaily) {
            spinner.setSelection(1)
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                isDaily =
                    spinner.selectedItem == resources.getStringArray(R.array.weather_display_array)[1]
                db.updateById(User.getInstance().getId().toString(), isDaily)
                if (!isFirstOpen) checkSpinner()
                else {
                    isFirstOpen = false
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        }

        val signOutBtn = findViewById<View>(R.id.sign_out_btn)
        signOutBtn.setOnClickListener {
            signOut()
        }

        cityTextView = findViewById(R.id.cityText)
        searchBtn = findViewById(R.id.searchBtn)
        searchEditText = findViewById(R.id.searchEditText)

        searchBtn.setOnClickListener {
            User.getInstance().setCity(searchEditText.text.toString().trim())
            userData?.setUserData(
                User.getInstance().getId().toString(),
                User.getInstance().getCity()
            )
            checkSpinner()
        }
        if (connection!!.hasConnection(this)) {
            getUserData(User.getInstance().getId().toString())
        } else {
            setExpListAdapterData()
        }
    }

    private fun checkData() {
        if (User.getInstance().getCity() == "null") {
            if (!getLocation?.checkPermissions()!!) {
                getLocation?.requestPermissions()
            } else {
                getLastLocation()
            }
        } else {
            checkSpinner()
        }
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        mFusedLocationClient!!.lastLocation
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful && task.result != null) {
                    mLastLocation = task.result

                    val geoCoder = Geocoder(this, Locale.getDefault())
                    val addresses: List<Address> = geoCoder.getFromLocation(
                        mLastLocation!!.latitude,
                        mLastLocation!!.longitude,
                        1
                    )
                    if (addresses.isNotEmpty()) User.getInstance().setCity(addresses[0].locality)
                        userData?.setUserData(
                            User.getInstance().getId().toString(),
                            User.getInstance().getCity()
                        )
                    checkSpinner()
                } else {
                    Log.w(TAG, "getLastLocation:exception", task.exception)
                    getLocation?.showMessage(getString(R.string.no_location_detected))
                }
            }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                grantResults.isEmpty() -> {
                    Log.i(TAG, "User interaction was cancelled.")
                }
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                    getLastLocation()
                }
                else -> {
                    getLocation?.showSnackbar(
                        R.string.permission_denied_explanation, R.string.settings,
                        View.OnClickListener {
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts(
                                "package",
                                BuildConfig.APPLICATION_ID, null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        })
                }
            }
        }
    }

    private fun setList(table: String) {
        val data = db.readData(table)
        labels = ArrayList()
        values = ArrayList()
        for (i in 0 until data.size) {
            User.getInstance().setCity(data[i].city.toString())
            cityTextView.text = data[i].city
            labels?.add(data[i].date)
            values?.add(data[i].value)
        }
        mLabels?.add(labels!!)
        mValues?.add(values!!)
    }

    private fun setExpListAdapterData() {
        labels = ArrayList()
        values = ArrayList()
        mLabels = ArrayList()
        mValues = ArrayList()
        if (isDaily) {
            if (db.tableExists(TABLE_TEMP_DAILY)) setList(TABLE_TEMP_DAILY)
            if (db.tableExists(TABLE_HUMID_DAILY)) setList(TABLE_HUMID_DAILY)
            if (db.tableExists(TABLE_WIND_DAILY)) setList(TABLE_WIND_DAILY)
        } else {
            if (db.tableExists(TABLE_TEMP)) setList(TABLE_TEMP)
            if (db.tableExists(TABLE_HUMID)) setList(TABLE_HUMID)
            if (db.tableExists(TABLE_WIND)) setList(TABLE_WIND)
        }
        if (mLabels?.size != 0) {
            val listView =
                findViewById<View>(R.id.weatherListView) as ExpandableListView
            val adapter = ExpListAdapter(applicationContext, mLabels!!, mValues!!)
            listView.setAdapter(adapter)
        }
    }

    private fun checkSpinner() {
        if (connection!!.hasConnection(this)) {
            if (isDaily) {
                getDailyWeatherData(User.getInstance().getCity())
            } else {
                getHourlyWeatherData(User.getInstance().getCity())
            }
        } else {
            setExpListAdapterData()
        }

    }

    private fun clearDB() {
        labels = ArrayList()
        values = ArrayList()
        mLabels = ArrayList()
        mValues = ArrayList()
        if (isDaily) {
            if (db.tableExists(TABLE_TEMP_DAILY)) db.deleteData(TABLE_TEMP_DAILY)
            if (db.tableExists(TABLE_HUMID_DAILY)) db.deleteData(TABLE_HUMID_DAILY)
            if (db.tableExists(TABLE_WIND_DAILY)) db.deleteData(TABLE_WIND_DAILY)
        } else {
            if (db.tableExists(TABLE_TEMP)) db.deleteData(TABLE_TEMP)
            if (db.tableExists(TABLE_HUMID)) db.deleteData(TABLE_HUMID)
            if (db.tableExists(TABLE_WIND)) db.deleteData(TABLE_WIND)
        }

    }

    private fun getHourlyWeatherData(city: String?) {
        apiService.getWeather(city)
            ?.enqueue(object : Callback<WeatherData?> {
                @RequiresApi(Build.VERSION_CODES.O)
                @SuppressLint(
                    "SetTextI18n", "UseCompatLoadingForDrawables", "ResourceType",
                    "SimpleDateFormat"
                )
                override fun onResponse(
                    call: Call<WeatherData?>,
                    response: Response<WeatherData?>
                ) {
                    if (response.isSuccessful) {
                        val formatter: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                        cityTextView.text = response.body()?.getCity()?.name
                        val mDataList: List<HourlyWeatherList>? = response.body()?.list
                        clearDB()
                        for (i in 0..3) {
                            for (data in mDataList!!) {
                                if (data.getDtTxt()!!
                                        .split(" ")[0] == formatter.format(Calendar.getInstance().time)
                                ) {
                                    val time = data.getDtTxt()!!.split(" ")[1]
                                    when (i) {
                                        0 -> {
                                            val temperature = WeatherLocal(
                                                city,
                                                time,
                                                data.getMain()?.temp?.toInt()
                                                    .toString() + resources.getString(
                                                    R.string.temp_value
                                                )
                                            )
                                            db.insertData(temperature, TABLE_TEMP)
                                        }
                                        1 -> {
                                            val humidity = WeatherLocal(
                                                city,
                                                time,
                                                data.getMain()?.humidity?.toString() + resources.getString(
                                                    R.string.humidity_value
                                                )
                                            )
                                            db.insertData(humidity, TABLE_HUMID)
                                        }
                                        2 -> {
                                            val wind = WeatherLocal(
                                                city,
                                                time,
                                                data.getWind()?.speed?.toInt()
                                                    .toString() + resources.getString(
                                                    R.string.wind_speed
                                                )
                                            )
                                            db.insertData(wind, TABLE_WIND)
                                        }
                                    }
                                }
                            }
                        }
                        setExpListAdapterData()
                    }
                }

                override fun onFailure(call: Call<WeatherData?>, t: Throwable) {
                    t.printStackTrace()
                }
            })
    }

    private fun getDailyWeatherData(city: String?) {
        apiService.getWeather(city)
            ?.enqueue(object : Callback<WeatherData?> {
                @RequiresApi(Build.VERSION_CODES.O)
                @SuppressLint("SetTextI18n", "UseCompatLoadingForDrawables", "ResourceType")
                override fun onResponse(
                    call: Call<WeatherData?>,
                    response: Response<WeatherData?>
                ) {
                    if (response.isSuccessful) {
                        cityTextView.text = response.body()?.getCity()?.name
                        val mDataList: List<HourlyWeatherList>? = response.body()?.list
                        clearDB()
                        for (i in 0..3) {
                            for (data in mDataList!!) {
                                if (data.getDtTxt()!!.split(" ")[1] == "21:00:00") {
                                    val date = data.getDtTxt()!!.split(" ")[0]
                                    when (i) {
                                        0 -> {
                                            val temperature = WeatherLocal(
                                                city,
                                                date,
                                                data.getMain()?.temp?.toInt()
                                                    .toString() + resources.getString(
                                                    R.string.temp_value
                                                )
                                            )
                                            db.insertData(temperature, TABLE_TEMP_DAILY)
                                        }
                                        1 -> {
                                            val humidity = WeatherLocal(
                                                city,
                                                date,
                                                data.getMain()?.humidity?.toString() + resources.getString(
                                                    R.string.humidity_value
                                                )
                                            )
                                            db.insertData(humidity, TABLE_HUMID_DAILY)
                                        }
                                        2 -> {
                                            val wind = WeatherLocal(
                                                city,
                                                date,
                                                data.getWind()?.speed?.toInt()
                                                    .toString() + resources.getString(
                                                    R.string.wind_speed
                                                )
                                            )
                                            db.insertData(wind, TABLE_WIND_DAILY)
                                        }
                                    }
                                }
                            }

                        }
                        setExpListAdapterData()
                    }
                }

                override fun onFailure(call: Call<WeatherData?>, t: Throwable) {
                    t.printStackTrace()
                }
            })
    }

    private fun getUserData(id: String) {
        val db = Firebase.firestore
        db.collection("users")
            .document(id)
            .get()
            .addOnSuccessListener { document ->
                User.getInstance().setCity(document.getString("city").toString())
                checkData()
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }

    }

    private fun getToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
                val msg = getString(R.string.msg_token_fmt, token)
                Log.d(TAG, msg)
            })
    }


    private fun signOut() {
        Firebase.auth.signOut()
        LoginManager.getInstance().logOut()
        db.deleteData(TABLE_USER)
        finish()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

}