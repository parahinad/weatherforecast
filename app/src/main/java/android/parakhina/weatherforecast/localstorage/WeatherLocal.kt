package android.parakhina.weatherforecast.localstorage

import com.google.firebase.auth.FirebaseUser


class WeatherLocal {

    var id: Int = 0

    var uId: String? = null

    var isDaily: Boolean = false

    var city: String? = null

    var date: String? = null

    var value: String? = null

    constructor(city: String?, date:String, value: String){
        this.city = city
        this.date = date
        this.value = value
    }

    constructor(uId: String?, isDaily: Boolean){
        this.uId = uId
        this.isDaily = isDaily
    }


    constructor(){
    }

}