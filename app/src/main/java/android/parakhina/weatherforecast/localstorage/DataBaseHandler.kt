package android.parakhina.weatherforecast.localstorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.parakhina.weatherforecast.model.User
import android.parakhina.weatherforecast.support.*
import android.util.Log
import android.widget.Toast
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class DataBaseHandler(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1){

    private val TAG = "DataBaseHandler"

    override fun onCreate(db: SQLiteDatabase?) {
       createTable(db, TABLE_TEMP)
        createTable(db, TABLE_HUMID)
        createTable(db, TABLE_WIND)
        createTable(db, TABLE_TEMP_DAILY)
        createTable(db, TABLE_HUMID_DAILY)
        createTable(db, TABLE_WIND_DAILY)
        val createTable = "CREATE TABLE " + TABLE_USER +" (" +
                COL_ID +" VARCHAR(256)," +
                COL_DAILY + " BOOLEAN)"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    fun tableExists( table: String?): Boolean {
        val db = this.writableDatabase
        if (table== null || db == null || !db.isOpen) {
            return false
        }
        val cursor: Cursor = db.rawQuery(
            "SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?",
            arrayOf("table", table)
        )
        if (!cursor.moveToFirst()) {
            cursor.close()
            return false
        }
        val count: Int = cursor.getInt(0)
        cursor.close()
        return count > 0
    }

    private fun createTable(db: SQLiteDatabase?, table: String){
        val createTable = "CREATE TABLE " + table +" (" +
                COL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_CITY + " VARCHAR(256)," +
                COL_DATE + " VARCHAR(256)," +
                COL_VALUE + " VARCHAR(256))"
        db?.execSQL(createTable)
    }

    fun insertData(weather: WeatherLocal, table: String){
        val db = this.writableDatabase
        var cv = ContentValues()
        if(table == "User"){
            cv.put(COL_ID, weather.uId)
            cv.put(COL_DAILY, weather.isDaily)
        }else {
            cv.put(COL_CITY, weather.city)
            cv.put(COL_DATE, weather.date)
            cv.put(COL_VALUE, weather.value)
        }
        var result = db.insert(table, null, cv)
        if(result == (-1).toLong())
            Log.w(TAG, "Failed adding to db")
        else
            Log.w(TAG, "Success adding to db")
    }

    fun readData(table: String) : MutableList<WeatherLocal>{
        var list : MutableList<WeatherLocal> = ArrayList()

        val db = this.readableDatabase
        val query = "Select * from $table"
        val result = db.rawQuery(query, null)
        if(result.moveToFirst()){
            do {
                var weather = WeatherLocal()
                if(table == "User"){
                    weather.uId = result.getString(result.getColumnIndex(COL_ID))
                    weather.isDaily = (result.getInt(result.getColumnIndex(COL_DAILY)) == 1)
                }else {
                    weather.id = result.getString(result.getColumnIndex(COL_ID)).toInt()
                    weather.city = result.getString(result.getColumnIndex(COL_CITY))
                    weather.date = result.getString(result.getColumnIndex(COL_DATE))
                    weather.value = result.getString(result.getColumnIndex(COL_VALUE))
                }

                list.add(weather)
            }while (result.moveToNext())
        }

        result.close()
        db.close()
        return list
    }

    fun updateById(id: String, isDaily: Boolean){
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COL_ID, id)
        cv.put(COL_DAILY, isDaily)
        val whereClause = "$COL_ID=?"
        val whereArgs = arrayOf(id.toString())
        var result = db.update(TABLE_USER, cv, whereClause, whereArgs)
        if(result == (-1).toInt())
            Log.w(TAG, "Update failed")
        else
            Log.w(TAG, "Update success")
    }

    fun deleteData(table: String){
        val db = this.writableDatabase
        db.delete(table, null, null)
        db.close()
    }

}