package android.parakhina.weatherforecast.support

internal const val RC_SIGN_IN = 1337
internal const val EMAIL = "email"
internal const val PROFILE = "public_profile"
internal const val ROOT_URL = "https://api.openweathermap.org/data/2.5/"
internal const val PACKAGE_NAME = "android.parakhina.weatherforecast"
internal const val RESULT_DATA_KEY = "$PACKAGE_NAME.RESULT_DATA_KEY"
internal const val RECEIVER = "$PACKAGE_NAME.RECEIVER"
internal const val LOCATION_DATA_EXTRA = "$PACKAGE_NAME.LOCATION_DATA_EXTRA"
internal const val SUCCESS_RESULT = 1
internal const val FAILURE_RESULT = 0
internal const val DATABASE_NAME ="WeatherDB"
internal const val TABLE_TEMP  = "Temperature"
internal const val TABLE_HUMID  = "Humidity"
internal const val TABLE_USER  = "User"
internal const val TABLE_WIND = "Wind"
internal const val TABLE_TEMP_DAILY  = "DailyTemperature"
internal const val TABLE_HUMID_DAILY  = "DailyHumidity"
internal const val TABLE_WIND_DAILY = "DailyWind"
internal const val COL_CITY = "city"
internal const val COL_ID = "id"
internal const val COL_DATE = "date"
internal const val COL_VALUE = "value"
internal const val COL_DAILY = "isDaily"
internal const val REQUEST_PERMISSIONS_REQUEST_CODE = 34