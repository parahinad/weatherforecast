package android.parakhina.weatherforecast.support

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.parakhina.weatherforecast.BuildConfig
import android.parakhina.weatherforecast.R
import android.parakhina.weatherforecast.firestore.UserData
import android.parakhina.weatherforecast.model.User
import android.parakhina.weatherforecast.view.activity.MainActivity
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import java.util.*

class GetPermission (mainActivity: MainActivity, context: Context){

    private val TAG = "MainActivity"
    private var mContext = context
    private var mMainActivity = mainActivity


    fun showMessage(text: String) {
        val container = mMainActivity.findViewById<View>(R.id.main_activity_container)
        if (container != null) {
            Toast.makeText(mContext, text, Toast.LENGTH_LONG).show()
        }
    }

    fun showSnackbar(
        mainTextStringId: Int, actionStringId: Int,
        listener: View.OnClickListener
    ) {

        Toast.makeText(mContext, mContext.resources.getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

     fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            mContext,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            mMainActivity,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            mMainActivity,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

            showSnackbar(
                R.string.permission_rationale, android.R.string.ok,
                View.OnClickListener {
                    startLocationPermissionRequest()
                })

        } else {
            Log.i(TAG, "Requesting permission")
            startLocationPermissionRequest()
        }
    }


}