package android.parakhina.weatherforecast.support

import android.app.IntentService
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.ResultReceiver
import android.text.TextUtils
import androidx.annotation.Nullable
import java.util.*
import kotlin.collections.ArrayList


class FetchAddressIntentService : IntentService("FetchAddressIntentService") {
    private var resultReceiver: ResultReceiver? = null
    override fun onHandleIntent(@Nullable intent: Intent?) {
        if (intent != null) {
            var errorMessage = ""
            resultReceiver = intent.getParcelableExtra(RECEIVER)
            val location: Location = intent.getParcelableExtra(LOCATION_DATA_EXTRA)
                ?: return
            val geocoder = Geocoder(this, Locale.getDefault())
            var addresses: List<Address>? = null
            try {
                addresses =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1)
            } catch (exception: Exception) {
                errorMessage = exception.message!!
            }
            if (addresses == null || addresses.isEmpty()) {
                deliverResultReceiver(FAILURE_RESULT, errorMessage)
            } else {
                val address: Address = addresses[0]
                val addressFragments: ArrayList<String?> = ArrayList()
                addressFragments.add(address.countryName)
                deliverResultReceiver(
                   SUCCESS_RESULT,
                    TextUtils.join(
                        Objects.requireNonNull(System.getProperty("line.separator").toString()),
                        addressFragments
                    )
                )
            }
        }
    }

    private fun deliverResultReceiver(resultCode: Int, addressMessage: String?) {
        val bundle = Bundle()
        bundle.putString(RESULT_DATA_KEY, addressMessage)
        resultReceiver?.send(resultCode, bundle)
    }
}
