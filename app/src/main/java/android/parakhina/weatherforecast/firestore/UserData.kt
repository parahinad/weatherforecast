package android.parakhina.weatherforecast.firestore

import android.parakhina.weatherforecast.model.User
import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase

class UserData {

    private val TAG = "UserData"
    private val db = Firebase.firestore

    fun setUserData(id: String, city: String?) {
        // Create a new user with a first and last name
        val user = hashMapOf(
            "city" to city
        )

        db.collection("users")
            .document(id)
            .set(user)
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot added with ID: $id")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }

    }

    fun getUserData(id: String) {
        db.collection("users")
            .document(id)
            .get()
            .addOnSuccessListener { document ->
                 User.getInstance().setCity(document.getString("city").toString())
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }
}