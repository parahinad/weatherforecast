package android.parakhina.weatherforecast.api

import android.parakhina.weatherforecast.model.ApiModel.WeatherData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {
    @GET("forecast?appid=328a4969311ff0f1504d2dbb95403f63&units=metric")
    fun getWeather(@Query("q") city: String?): Call<WeatherData?>?
}