package android.parakhina.weatherforecast.api

import android.parakhina.weatherforecast.support.ROOT_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RetroClient {
    companion object RetroClient {
        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ROOT_URL)
                .build()

            return retrofit.create(ApiService::class.java);
        }
    }

}